"""
A library for reading from a MAX17043 lithium battery fuel gauge over I2C

"""
from __future__ import print_function

import Adafruit_I2C 

import argparse
import datetime
import time

I2C_BUS = 1
FUEL_ADDRESS = 0x36

VCELL_REGISTER   = 0x02
SOC_REGISTER     = 0x04
MODE_REGISTER    = 0x06
VERSION_REGISTER = 0x08
CONFIG_REGISTER  = 0x0C
COMMAND_REGISTER = 0xFE

wire = Adafruit_I2C.Adafruit_I2C(0x36, busnum=I2C_BUS)

def writeReset():
    """ reset the fuel guage """
    return writeRegister(COMMAND_REGISTER, 0x00, 0x54)

def writeQuickStart():
    """ set the fuel guage to quick start mode """
    return writeRegister(MODE_REGISTER, 0x40, 0x00)

def writeAlertThreshold(threshold):
    """ set the alert threshold % between 1 and 32 """
    msb, lsb = readRegister(CONFIG_REGISTER)
    threshold = max(1, min(32, threshold)) # threshold between 1 & 32
    threshold = 32 - threshold
    return writeRegister(CONFIG_REGISTER, msb, (lsb & 0xE0) | threshold)

def writeAlertReset():
    """ reset the alert bit to 0 """
    msb, lsb = readRegister(CONFIG_REGISTER)
    return writeRegister(CONFIG_REGISTER, msb, (lsb & ~(0x20)))

def readVCell():
    """ read the voltage of the battery """
    msb, lsb = readRegister(VCELL_REGISTER)
    value = (msb << 4) | (lsb >> 4)
    return mapValue(value, 0x000, 0xFFF, 0, 50000) / 10000.0

def readSOC():
    """ read the "state of charge" (% remaining) of the battery """
    msb, lsb = readRegister(SOC_REGISTER)
    return msb + lsb/256.0

def readVersion():
    """ read the fuel guage version """
    msb, lsb = readRegister(VERSION_REGISTER)
    return (msb << 8) | lsb

def readCompensatedValue():
    """ read the battery chemistry compensated value """
    msb, lsb = readRegister(CONFIG_REGISTER)
    return msb

def readAlertThreshold():
    """ read the current alert threshold (%) """
    msb, lsb = readRegister(CONFIG_REGISTER)
    return 32 - (lsb & 0x1F)

def readAlert():
    """
    read whether or not the battery is "in alert" / below the alert threshold

    """
    msb, lsb = readRegister(CONFIG_REGISTER)
    return (lsb & 0x20 == 0x20)

def readRegister(address):
    """ utility function for reading 2 bytes from a register address """
    return wire.readList(address, 2)

def writeRegister(address, msb, lsb):
    """ utility function for writing 2 bytes to a register address """
    return wire.writeList(address, [msb, lsb])

def mapValue(x, in_min, in_max, out_min, out_max):
    """ utility function for mapping a value from one range to another """
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min

def watch(interval=15, eol_percent=10, max_n=120):
    """
    watch the battery consumption using a modified moving average of the
    state of charge and estimate the time remaining at the current usage.

    """
    prev_soc = None
    diff_mma = 0
    diff_n = 0
    charging = False

    while True:
        # get the current state of charge
        soc = readSOC()
        if prev_soc is None:
            prev_soc = soc

        # compute the difference between the current and previous soc reading
        diff = soc - prev_soc
        if diff <= 0:
            charging = False
        else:
            charging = True

        # compute a modified moving average of the rate of usage
        diff_mma = mma(diff, mma_yesterday=diff_mma, n=diff_n)
        diff_n = min(diff_n + 1, max_n) # only weight max_n observations (this should helps with large changes in power consumption from going unnoticed)
        prev_soc = soc

        if diff_mma != 0:
            ttl = ((soc-eol_percent)/-diff_mma)*interval # how many seconds until we reach 10%?
            h, m, s = split_seconds(ttl)
            print("estimated time left: {0}h {1}m {2}s ({3:.2f}%){4}".format(int(h), int(m), int(s), soc, " **charging**" if charging else ""))

        # sleep until the next reading
        time.sleep(interval)

def mma(observation, mma_yesterday=0, n=0):
    """
    Compute the modified moving average for a series of observations.
    
    Formally:

        MMAtoday = (((N-1) * MMAyesterday) + observation) / N

        (which is, in short, an exponential moving average with alpha=1/N)
    
    """
    if n == 0:
        return observation
    return (((n-1) * mma_yesterday) + observation) / (n)

def split_seconds(seconds):
    """ split seconds into hours, minutes, and seconds """
    seconds = int(seconds)
    if seconds < 0:
        return 0, 0, 0
    hours = seconds/3600
    seconds = seconds % 3600
    minutes = seconds / 60
    seconds = seconds % 60
    return hours, minutes, seconds

def dump():
    """ dump the current state to stdout """
    print("timestamp: {0}".format(datetime.datetime.now()))
    print("version: {0}".format(readVersion()))
    print("compensated: {0}".format(readCompensatedValue()))
    print("threshold: {0}".format(readAlertThreshold()))
    print("alert: {0}".format(readAlert()))
    print("vcell: {0}".format(readVCell()))
    print("soc: {0:.2f}%".format(readSOC()))

if __name__ == "__main__":
    try:
        watch()
    except KeyboardInterrupt:
        print()
