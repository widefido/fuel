A python I2C library for reading from a MAX17043 lithium battery fuel gauge  
  
- for the BeagleBone Black and Raspberry Pi
- built atop the python Adafruit_I2C I/O library